# Surströmming Project Setup
Surströmming is a simple project setup including a SCSS-base to deliver a unified approach for various projects.

* Mobile-first breakpoint mixins
* Grid System based on BEM
* _helpful_ Sass Helpers


### How to

* Compass is included in Surströmming, so one thing less to worry about.
* Adjust the given `_variables.scss` file in the root directory to your needs and include it before Surströmming
* That’s basically it


### Grid System

The grid system looks fairly complex from the code, but comes down to the simpler concepts of equally sized and preset-width columns.
To make handling the grid easier, I added a smaller set of breakpoints for the grid to adjust. Mobile devices only display one column, and from tablet up, it's gonna grow relative to the maximum number of columns set in the constants collection.
Modifying the grid works primarily through the constants - no magic numbers floating around anywhere, the functions adjust the CSS to your needs.

Either tag along with the simple (equally sized) `.grid__unit` or do some maths in your head with `.grid__unit.grid__unit--span-x` (pro tip: the columns should usually add up to something that looks like your set grid columns).


### Fancy stuff

* To allow for some styling that's _only_ useful for development and which shouldn't be used at all for production: Introducing `@mixin in-development()`. Add styling inside the mixin call and don't worry about forgetting about it, because you'll set the corresponding boolean `$in-development` to `false` when the time comes and all that styling will be gone. I don't know if anyone else thinks this is handy, but I always play with colors in development.